<?php

namespace Drupal\jedi_console;

/**
 * Class Lando.
 *
 * @package Drupal\jedi_console
 */
class Lando {

  /**
   * Lando ENV information.
   *
   * @var array
   */
  protected static $landoInfo;

  /**
   * Returns Lando info.
   *
   * @return array
   */
  public static function getInfo() {
    if (!static::$landoInfo && getenv('LANDO') === 'ON') {
      static::$landoInfo = json_decode(getenv('LANDO_INFO'), TRUE);;
    }
    return static::$landoInfo;
  }

}
