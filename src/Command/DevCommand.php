<?php

namespace Drupal\jedi_console\Command;

use Drupal\Console\Core\Utils\ChainQueue;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Core\Site\Settings;

/**
 * Class DevCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="jedi_console",
 *     extensionType="module"
 * )
 */
class DevCommand extends ContainerAwareCommand {

  /**
   * @var ChainQueue
   */
  protected $chainQueue;

  /**
   * Constructor.
   *
   * @param ChainQueue $chain_queue
   */
  public function __construct(ChainQueue $chain_queue) {
    $this->chainQueue = $chain_queue;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('jedi:dev')
      ->setDescription('Jedi Console development command.')
      ->setHelp('Install modules excluded from configuration and set common settings for development.');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $excluded = Settings::get('config_exclude_modules');
    if ($excluded) {
      $this->chainQueue->addCommand('module:install', ['module' => $excluded], FALSE);
    }
  }

}
