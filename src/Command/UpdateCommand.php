<?php

namespace Drupal\jedi_console\Command;

use Drupal\Console\Core\Utils\ChainQueue;
use Drupal\Console\Core\Utils\ShellProcess;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Drupal\Console\Core\Command\ContainerAwareCommand;

/**
 * Class UpdateCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="jedi_console",
 *     extensionType="module"
 * )
 */
class UpdateCommand extends ContainerAwareCommand {

  /**
   * @var ShellProcess
   */
  protected $shellProcess;
  /**
   * @var ChainQueue
   */
  protected $chainQueue;

  /**
   * Constructor.
   *
   * @param ShellProcess $shell_process
   * @param ChainQueue $chain_queue
   */
  public function __construct(ShellProcess $shell_process, ChainQueue $chain_queue) {
    $this->shellProcess = $shell_process;
    $this->chainQueue = $chain_queue;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('jedi:update')
      ->setAliases(['jedi:up'])
      ->setDescription('Jedi Console update command.')
      ->setHelp('Install dependencies via composer, execute database updates, and imports the sync configuration. Defaults to running all updates, or specific options.')
      ->addOption(
        'db',
        NULL,
        InputOption::VALUE_NONE,
        'Run Drupal database updates.'
      )
      ->addOption(
        'composer',
        NULL,
        InputOption::VALUE_NONE,
        'Install composer dependencies.'
      )
      ->addOption(
        'config',
        NULL,
        InputOption::VALUE_NONE,
        'Import local configuration.'
      )
    ;
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    try {
      $updateAll = TRUE;
      if($input->getOption('db') || $input->getOption('composer') || $input->getOption('config')) {
        $updateAll = FALSE;
      }

      if ($input->getOption('composer') || $updateAll) {
        $this->getIo()->commentBlock('Running composer install ...');
        $composerRoot = $this->drupalFinder->getComposerRoot();
        $this->shellProcess->exec('composer install', $composerRoot);
      }

      if ($input->getOption('db') || $updateAll) {
        $this->getIo()->commentBlock('Running database updates ...');
        $this->chainQueue->addCommand('cache:rebuild', ['cache' => 'all'], FALSE);
        $this->chainQueue->addCommand('update:execute', [], FALSE);
      }

      if ($input->getOption('config') || $updateAll) {
        $this->getIo()->commentBlock('Importing local configuration ...');
        $this->chainQueue->addCommand('config:import', [], FALSE);
        $this->chainQueue->addCommand('cache:rebuild', ['cache' => 'all'], FALSE);
      }
    }
    catch (\Exception $exception) {
      throw $exception;
    }
  }

}
