<?php

namespace Drupal\jedi_console\Command;

use Drupal\Console\Command\Shared\ConfirmationTrait;
use Drupal\Console\Core\Utils\ShellProcess;
use Drush\Drush;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class PullCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="jedi_console",
 *     extensionType="module"
 * )
 */
class PullCommand extends ContainerAwareCommand {

  use ConfirmationTrait;

  /**
   * @var ShellProcess
   */
  protected $shellProcess;

  /**
   * Constructor.
   *
   * @param ShellProcess $shell_process
   */
  public function __construct(ShellProcess $shell_process) {
    $this->shellProcess = $shell_process;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('jedi:pull')
      ->setDescription('Jedi Console pull database command.')
      ->setHelp('Sync a remote database with the local database')
      ->addArgument(
        'alias',
        InputArgument::OPTIONAL,
        'The remote Drush alias.',
        '@prod'
      )
    ;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $question = sprintf(
      '<fg=black;bg=yellow>This is a destructive action. The local database will be dropped and replaced with the %s database, are you sure?</>',
      $input->getArgument('alias')
    );
    $confirmation = $this->getIo()->confirm($question, TRUE);
    if (!$confirmation) {
      $this->getIo()->warning('Canceled');
      return 1;
    }

    $drupalRoot = $this->drupalFinder->getDrupalRoot();
    $alias = $input->getArgument('alias');
    // Test the Drush alias existins
    $this->shellProcess->exec(sprintf('drush site:alias %s --root=%s', $alias, $drupalRoot), $drupalRoot);

    // Drop the local database.
    // SQL Sync only syncs missing database values, and doesn't remove tables/columns that exist only in the dev env.
    // For that reason, it's better to drop the local database, import the remote, and then import configuration.
    $this->shellProcess->exec(sprintf('drush @self sql:drop --root=%s --yes', $drupalRoot), $drupalRoot);
    // Sync database.
    $this->shellProcess->exec(sprintf('drush sql-sync %s @self --root=%s --yes', $alias, $drupalRoot), $drupalRoot);
  }

}
