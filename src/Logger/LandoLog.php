<?php

namespace Drupal\jedi_console\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;

/**
 * Class LandoLog.
 *
 * @package Drupal\jedi_console
 */
class LandoLog implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * A configuration object containing system.logging settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a MyLog object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser, DateFormatterInterface $date_formatter) {
    $this->config = $config_factory->get('syslog.settings');
    $this->parser = $parser;
    $this->dateFormatter = $date_formatter;

  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);
    // Send logs to PHP stream so they can be read in Lando `etc logs` command.
    if ($level < RfcLogLevel::NOTICE) {
      $output = fopen('php://stderr', 'w');
    }
    else {
      $output = fopen('php://stdout', 'w');
    }
    fwrite($output, sprintf('%s: [%s] %s: %s | link: %s | uid: %s | uri: %s | referer: %s | date: %s %s',
      $this->config->get('identity'),
      strtoupper((string) RfcLogLevel::getLevels()[$level]),
      strip_tags($message),
      $context['channel'],
      strip_tags($context['link']),
      $context['uid'],
      $context['request_uri'],
      $context['referer'],
      $this->dateFormatter->format($context['timestamp']),
      PHP_EOL
    ));
    fclose($output);
  }

}
