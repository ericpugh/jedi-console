# Jedi Console

A Drupal module providing utilities for developing with [Lando](https://docs.lando.dev/)

## Install
It's recommend to install in a Drupal project with [composer](https://getcomposer.org/).

```
composer require ericpugh/jedi-console --dev
```

## Utilities

- Alter the core Syslog service to make Drupal log events available within `lando logs` command.

This makes it easier to work with logs, for example you could change the "identity" in Drupal Syslog's settings to "JEDI" and then follow the logs with a command like this:
```shell script
lando logs -f -s appserver | grep -i 'jedi'
```
- A class to retrieve Lando environment information.

Example:
```php
$lando = \Drupal\jedi_console\Lando::getInfo();
$nodeVersion = $lando['nodejs']['version'];
echo $nodeVersion;
// outputs: 10
```

## Console Commands

Jedi console provides several [Drupal Console](https://github.com/hechoendrupal/drupal-console) commands for common tasks while working in a local environment.

- `drupal jedi:pull` pulls a remote database, with an arguement for the Drush site alias. (note: Expects Drush aliases to be properly configured, see [example](https://github.com/drush-ops/drush/blob/master/examples/example.site.yml).)
- `drupal jedi:up` Updates a local site. Runs cache clear, update database, and configuration import.
- `drupal jedi:dev` get the modules listed in Drupal's `$settings['config_exclude_modules']` and enable those modules.

Please [create and issue](https://gitlab.com/ericpugh/jedi-console/-/issues) or submit a Merge Request for any improvements or ideas for useful features.


